
#include <iostream>
#include <string>

class Player
{
private:

    std::string Name;
    int score = 0;

public:

    std::string GetName() { return Name; }
    int GetScore() { return score; }
    void SetName(std::string n) { Name = n; }
    void SetScore(int s) { score = s; }

    void Show()
    {
        std::cout << Name << " - " << score << '\n';
    }
    Player()
    {}
};

void sort(Player* arr, int n)
{
    bool a = true;
    while (a)
    {
        a = false;
        for (int i = 0; i < n - 1; i++)
        {
            if (arr[i].GetScore() < arr[i+1].GetScore())
            {
                std::swap(arr[i], arr[i + 1]);
                a = true;
            }
        }
    }
}


int main()
{
    int n;
    std::cout << "Enter Number of players : ";
    std::cin >> n;
    Player* arr = new Player[n];

    for (int i = 0; i < n; i++)
    {

        std::string Name;
        std::cout << "Enter Player Name : ";
        std::cin >> Name;

        int score;
        std::cout << "Enter his score : ";
        std::cin >> score;

        arr[i].SetName(Name);
        arr[i].SetScore(score);
    }
    
    sort(arr, n);

    for (int i = 0; i < n; i++)
    {
        std::cout << arr[i].GetName() << ' ' << arr[i].GetScore() << '\n';
    }

    delete[] arr;
    arr = nullptr;
    
}
